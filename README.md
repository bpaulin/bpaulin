![Skills banner](skills-banner.png)

 * Human since 1980
 * Developer since 1997
 * Linux user since 2002
 * Lyonnais since 2012
 * Husband since 2014
 * Dad since 2017

![bpaulin Github stats](https://github-readme-stats.vercel.app/api?username=bpaulin&show_icons=true&hide_border=true&count_private=true&show_icons=true)
